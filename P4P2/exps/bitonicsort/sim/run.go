package main

import (
	"flag"

	_ "net/http/pprof"

	"gitlab.com/akita/mgpusim/v2/benchmarks/amdappsdk/bitonicsort"
	"gitlab.com/yuhuibao/instances/P2/runner"
)

var length = flag.Int("length", 1024, "The length of array to sort")

func main() {
	flag.Parse()

	runner := new(runner.Runner).ParseFlag().Init()

	benchmark := bitonicsort.NewBenchmark(runner.Driver())
	benchmark.Length = *length
	runner.AddBenchmark(benchmark)
	runner.Run()
}
