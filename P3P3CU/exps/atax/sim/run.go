package main

import (
	"flag"

	_ "net/http/pprof"

	"gitlab.com/akita/mgpusim/v2/benchmarks/polybench/atax"
	"gitlab.com/yuhuibao/instances/P3CU/runner"
)

var xFlag = flag.Int("x", 4096, "The height of the matrix")
var yFlag = flag.Int("y", 4096, "The width of the matrix")

func main() {
	flag.Parse()

	runner := new(runner.Runner).ParseFlag().Init()

	benchmark := atax.NewBenchmark(runner.Driver())
	benchmark.NX = *xFlag
	benchmark.NY = *yFlag
	runner.AddBenchmark(benchmark)
	runner.Run()
}
