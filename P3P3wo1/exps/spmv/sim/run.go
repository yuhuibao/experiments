package main

import (
	"flag"

	"gitlab.com/akita/mgpusim/v2/benchmarks/shoc/spmv"
	"gitlab.com/yuhuibao/instances/P3wo1/runner"
)

var path = flag.String("load-csr", "", "Path to file from which csr format matrix to be loaded.")

// Dim is dimension
var Dim = flag.Int("dim", 128, "The number of rows in the input matrix.")

// Sparsity is sparsity
var Sparsity = flag.Float64("sparsity", 0.01,
	"The ratio between non-zero elements to all the elelements in the matrix")

func main() {
	flag.Parse()

	runner := new(runner.Runner).ParseFlag().Init()

	benchmark := spmv.NewBenchmark(runner.Driver())
	benchmark.Dim = int32(*Dim)
	benchmark.Sparsity = *Sparsity
	benchmark.Path = *path

	runner.AddBenchmark(benchmark)

	runner.Run()
}
