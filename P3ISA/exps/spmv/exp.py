import re
import os
from termcolor import colored
import subprocess
import pandas as pd
import traceback
from threading import Lock
from multiprocessing.pool import ThreadPool


def compile_simulation(path):

    p = subprocess.Popen("go build", shell=True, cwd=path)
    p.wait()
    if p.returncode == 0:
        print(colored("Compiled " + path, "green"))
        return False
    else:
        print(colored("Compile failed " + path, "red"))
        return True


def run_sim(thread_pool, gpu_conf, path, name, data):
    param = [i for i in range(128, 2049, 128)]
    lock = Lock()
    for p in param:
        args = (
            path,
            "./spmv -load-csr={0}.data" + gpu_conf,
            p,
            name,
            data,
            lock,
        )
        result = thread_pool.apply_async(run_simulation, args)


def run_simulation(path, cmd, param, name, data, lock):
    cmd = cmd.format(param)
    process = subprocess.Popen(cmd, shell=True, cwd=path)
    process.wait()
    if process.returncode != 0:
        print("Error executing ", cmd)
    else:
        print("Executed ", cmd)

    try:
        time = parse_kernel_time_sim(path + "/metrics.csv")
    except Exception as e:
        print("Error parsing kernel time: ", e)
        traceback.print_exc()
    entry = [name, param, time]
    lock.acquire()
    data.loc[len(data)] = entry
    lock.release()


def parse_kernel_time_sim(filename):
    pattern = re.compile(r"kernel_time,([0-9.]+)")
    fp = open(filename, "r")
    content = fp.read()
    match = pattern.search(content)

    if match is None:
        raise Exception("Error parsing kernel time")

    return float(match.group(1))


def run(name, gpu_conf, data):
    tp = ThreadPool()
    curr_path = os.path.dirname(os.path.realpath(__file__))
    compile_simulation(curr_path)
    run_sim(tp, gpu_conf, curr_path, name, data)
    tp.close()
    tp.join()


def main():
    data = pd.DataFrame(columns=["run", "param", "time"])
    gpu_conf = " -gpu=RX5500XT -isa=gfx1010"
    run("baseline", gpu_conf, data)
    gpu_conf = " -gpu=RX5500XT -isa=gfx803"
    run("sim", gpu_conf, data)
    data.to_csv("results.csv")


if __name__ == "__main__":
    main()
