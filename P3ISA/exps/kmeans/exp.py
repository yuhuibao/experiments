import re
import os
from termcolor import colored
import subprocess
import pandas as pd
import traceback


def compile_simulation(path):

    p = subprocess.Popen("go build", shell=True, cwd=path)
    p.wait()
    if p.returncode == 0:
        print(colored("Compiled " + path, "green"))
        return False
    else:
        print(colored("Compile failed " + path, "red"))
        return True


def run_simulation(gpu_conf, path, name, data):
    param = [i for i in range(256, 4097, 256)]

    for p in param:
        cmd = "./kmeans -features=64 -points=" + str(p) + gpu_conf
        process = subprocess.Popen(cmd, shell=True, cwd=path)
        process.wait()
        if process.returncode != 0:
            print("Error executing ", cmd)
        else:
            print("Executed ", cmd)

        try:
            time = parse_kernel_time_sim(path + "/metrics.csv")
        except Exception as e:
            print("Error parsing kernel time: ", e)
            traceback.print_exc()
        entry = [name, p, time]
        data.loc[len(data)] = entry


def parse_kernel_time_sim(filename):
    pattern = re.compile(r"kernel_time,([0-9.]+)")
    fp = open(filename, "r")
    content = fp.read()
    match = pattern.search(content)

    if match is None:
        raise Exception("Error parsing kernel time")

    return float(match.group(1))


def run(name, gpu_conf, data):
    curr_path = os.path.dirname(os.path.realpath(__file__))
    compile_simulation(curr_path)
    run_simulation(gpu_conf, curr_path, name, data)


def main():
    data = pd.DataFrame(columns=["run", "param", "time"])
    gpu_conf = " -gpu=RX5500XT -isa=gfx1010"
    run("baseline", gpu_conf, data)
    gpu_conf = " -gpu=RX5500XT -isa=gfx803"
    run("sim", gpu_conf, data)
    data.to_csv("results.csv")


if __name__ == "__main__":
    main()
