module gitlab.com/yuhuibao/experiments

go 1.16

replace gitlab.com/akita/mgpusim/v2 => ../../akita/mgpusim

replace gitlab.com/yuhuibao/instances => ../instances

require (
	gitlab.com/akita/mgpusim/v2 v2.0.0-00010101000000-000000000000
	gitlab.com/yuhuibao/instances v0.0.0-20220227220955-66f5326a2357
)
